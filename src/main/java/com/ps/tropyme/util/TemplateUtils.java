package com.ps.tropyme.util;

import java.io.StringWriter;
import java.util.HashMap;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

public class TemplateUtils {

	public static String getBodySolicitudTropyme(HashMap<String, Object> map)  {
		MustacheFactory mf = new DefaultMustacheFactory();
		Mustache template = mf.compile("template.solicitud.tropyme.mustache");
		StringWriter writer = new StringWriter();
		template.execute(writer, map);
		return writer.toString();
	}
	
}
