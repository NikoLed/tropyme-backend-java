package com.ps.tropyme.util;

import java.util.Date;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtUtils {
	
	private static final String SECRET = "PROVINCIA_SEGUROS_TROPYME";
	private static final long EXPIRATIONTIME = 864000000;
	
	public static String getToken(String productor) {
		return Jwts.builder().setSubject(productor)
		.setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
		.signWith(SignatureAlgorithm.HS512, SECRET).compact();
	}
}
