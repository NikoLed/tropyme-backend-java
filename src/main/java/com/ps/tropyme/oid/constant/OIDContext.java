package com.ps.tropyme.oid.constant;

import java.util.Hashtable;

import javax.annotation.PostConstruct;
import javax.naming.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class OIDContext {

	// se obtienen desde el archivo de properties
	@Value("${oid.url.server}")
	private String urlServer;

	@Value("${oid.ldap.context}")
	private String ldapContext;

	@Value("${oid.ldap.url.server}")
	private String ldapAdServer;

	@Value("${oid.ldap.admin.user}")
	private String ldapUsername;

	@Value("${oid.ldap.admin.pass}")
	private String ldapPassword;

	@Value("${oid.ldap.authentication}")
	private String ldapAuthentication;

	@Value("${oid.ldap.binary}")
	private String ldapBinary;

	@Value("${oid.bd.datasource}")
	private String bdDataSource;

	// se configuran en base al ldapAdServer y ldapContext
	private String ldapInitialBase;
	private String ldapSearchBase;
	private String ldapCreateBase;
	private String ldapGroupBase;

	private Hashtable<String, Object> context = new Hashtable<String, Object>();

	private static final Logger logger = LoggerFactory.getLogger(OIDContext.class);

	@PostConstruct
	public void initIt() throws Exception {
		this.getOIDInitialContext();
	}

	public Hashtable<String, Object> getOIDInitialContext() throws Exception {
		this.loadOIDProperties();
		return context;
	}

	private void loadOIDProperties() {
		try {

			logger.info("loadOIDProperties ----------------------------------------------------------------------");
			logger.info("loadOIDProperties - Server    : " + this.ldapAdServer);
			logger.info("loadOIDProperties - Contexto  : " + this.ldapContext);
			logger.info("loadOIDProperties - Usuario   : " + this.ldapUsername);
			logger.info("loadOIDProperties - Password  : " + this.ldapPassword);
			logger.info("loadOIDProperties - Datasource: " + this.bdDataSource);
			logger.info("loadOIDProperties ----------------------------------------------------------------------");

			// OID para login
			this.ldapInitialBase = "cn=Users, dc=" + ldapContext + ",dc=pseguros,dc=com";

			// OID para operaciones sobre usuarios
			ldapSearchBase = "cn=users,dc=" + ldapContext + ",dc=pseguros,dc=com";
			this.ldapCreateBase = "cn=Users,dc=" + ldapContext + ",dc=pseguros,dc=com";

			// OID para operaciones sobre grupos
			this.ldapGroupBase = "cn=groups,dc=" + ldapContext + ",dc=pseguros,dc=com";

			// actualizo contexto LDAP
			if (this.context == null)
				this.context = new Hashtable<String, Object>();

			this.context.put(Context.SECURITY_AUTHENTICATION, this.ldapAuthentication);
			this.context.put(Context.SECURITY_PRINCIPAL, "cn=" + this.ldapUsername + "," + this.ldapInitialBase);
			this.context.put(Context.SECURITY_CREDENTIALS, this.ldapPassword);
			this.context.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			this.context.put(Context.PROVIDER_URL, ldapAdServer);
			this.context.put("java.naming.ldap.attributes.binary", this.ldapBinary);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getLdapInitialBase() {
		return ldapInitialBase;
	}

	public String getLdapSearchBase() {
		return ldapSearchBase;
	}

	public String getLdapCreateBase() {
		return ldapCreateBase;
	}

	public String getLdapGroupBase() {
		return ldapGroupBase;
	}

	// -- getters & setters
	// -------------------------------------------------------

	public String getLdapContext() {
		return ldapContext;
	}

	public void setLdapContext(String ldapContext) {
		this.ldapContext = ldapContext;
	}

	public String getLdapAdServer() {
		return ldapAdServer;
	}

	public void setLdapAdServer(String ldapAdServer) {
		this.ldapAdServer = ldapAdServer;
	}

	public String getLdapUsername() {
		return ldapUsername;
	}

	public void setLdapUsername(String ldapUsername) {
		this.ldapUsername = ldapUsername;
	}

	public String getLdapPassword() {
		return ldapPassword;
	}

	public void setLdapPassword(String ldapPassword) {
		this.ldapPassword = ldapPassword;
	}

	public String getLdapAuthentication() {
		return ldapAuthentication;
	}

	public void setLdapAuthentication(String ldapAuthentication) {
		this.ldapAuthentication = ldapAuthentication;
	}

	public String getLdapBinary() {
		return ldapBinary;
	}

	public void setLdapBinary(String ldapBinary) {
		this.ldapBinary = ldapBinary;
	}

	public String getBdDataSource() {
		return bdDataSource;
	}

	public void setBdDataSource(String bdDataSource) {
		this.bdDataSource = bdDataSource;
	}

	// ----------------------------------------------------------------------------
}
