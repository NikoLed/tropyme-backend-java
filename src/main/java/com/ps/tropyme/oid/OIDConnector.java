package com.ps.tropyme.oid;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ps.tropyme.oid.constant.OIDContext;
import com.ps.tropyme.oid.vo.UsuarioOIDVO;

@Component
public class OIDConnector {

	@Autowired
	private OIDContext context;

	private static OIDConnector instance;

	private static final Logger logger = LoggerFactory.getLogger(OIDConnector.class);

	// ----------------------------------------------------------------------------

	public OIDConnector() {
		instance = this;
	}

	public static OIDConnector getInstance() {
		return (instance != null ? instance : new OIDConnector());
	}

	public boolean existeUsuario(String usuario) throws Exception {
		DirContext dirContext = null;

		try {
			logger.debug("Verificando existencia del usuario en OID: " + usuario);
			dirContext = new InitialDirContext(context.getOIDInitialContext());
			return (dirContext.getAttributes("cn=" + usuario + "," + context.getLdapSearchBase()) != null);
		} catch (javax.naming.NameNotFoundException nntfe) {
			logger.debug("No existe en OID el usuario: " + usuario);
			return false;
		} catch (Exception e) {
			logger.debug("Error al buscar en OID el usuario: " + usuario);
			e.printStackTrace();
			throw e;
		} finally {
			if (dirContext != null)
				dirContext.close();
		}
	}

	// -- get context
	// -------------------------------------------------------------

	public UsuarioOIDVO getUsuarioFromOID(UsuarioOIDVO usuario) throws Exception {
		Attributes attr = this.getUsuarioAttributes(usuario.getCodigo(), false);

		try {
			usuario.setCodigo((String) attr.get("cn").get());
		} catch (Exception e1) {
		}
		try {
			usuario.setCodigoProductor((String) attr.get("employeetype").get());
		} catch (Exception e2) {
		}
		try {
			usuario.setCodigoEntidad((String) attr.get("businesscategory").get());
		} catch (Exception e3) {
		}
		try {
			usuario.setCodigoPtoVenta((String) attr.get("destinationindicator").get());
		} catch (Exception e4) {
		}
		try {
			usuario.setNombre((String) attr.get("sn").get());
		} catch (Exception e5) {
		}
		try {
			usuario.setEmail((String) attr.get("mail").get());
		} catch (Exception e6) {
		}
		try {
			usuario.setCodigoProveedor((String) attr.get("internationalisdnnumber").get());
		} catch (Exception e7) {
		}
		try {
			usuario.setEsJerarquico((String) attr.get("carlicense").get());
		} catch (Exception e8) {
		}
		try {
			usuario.setCodigoUsuarioEntidad((String) attr.get("ou").get());
		} catch (Exception e9) {
		}
		try {
			usuario.setClave((String) attr.get("userpassword").get());
		} catch (Exception e9) {
		}
		try {
			usuario.setCodigoCliente((String) attr.get("o").get());
		} catch (Exception e9) {
		}

		return usuario;
	}

	private Attributes getUsuarioAttributes(String usuario, boolean printInLog) throws Exception {
		Attributes attributes = null;
		DirContext dirContext = null;

		try {
			dirContext = new InitialDirContext(context.getOIDInitialContext());
			usuario = usuario.toLowerCase(); // para que lo reconzca OID

			if (!existeUsuario(usuario))
				throw new Exception("El usuario " + usuario + " NO se encuentra dado de alta en OID");

			attributes = dirContext.getAttributes("cn=" + usuario + "," + context.getLdapSearchBase());

			if (printInLog) {
				for (NamingEnumeration<? extends Attribute> attrs = attributes.getAll(); attrs.hasMore();) {
					Attribute attr = (Attribute) attrs.next();
					logger.debug("Atributos del usuario: " + usuario + " - Atributo: [" + attr.getID() + "] - Valor: ["
							+ attr.get() + "]");
				}
			}
		} catch (Exception e) {
			logger.debug("Error al buscar obtener los atributos OID del usuario: " + usuario);
			e.printStackTrace();
			throw e;
		} finally {
			if (dirContext != null)
				dirContext.close();
		}
		return attributes;
	}

}
