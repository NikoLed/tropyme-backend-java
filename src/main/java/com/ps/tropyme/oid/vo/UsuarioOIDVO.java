package com.ps.tropyme.oid.vo;

import java.util.HashMap;

public class UsuarioOIDVO {

	private String codigo; // id de usuario en OID
	private String codigoCliente; // id de usuario en OID

	private String codigoOrigen; // para clonar un usuario desde otro usuario
	// origen
	private String codigoProductor;// para agentes
	private String codigoEntidad; // para mater dominus
	private String codigoPtoVenta; // para mater dominus
	private String codigoProveedor; // para proveedores
	private String codigoUsuarioEntidad;
	private String esJerarquico;
	private String clave;
	private String nombre;
	private String sucursal;
	private String sucursalId;
	private String email;
	private String rol;
	private String operacion;

	// posibles perfiles de un usuario
	private HashMap<String, String> perfiles;

	// para errores
	private String errorCodigo;
	private String errorMensaje;

	public UsuarioOIDVO(UsuarioOIDVOBuilder usuarioOIDVOBuilder) {
		this.esJerarquico = usuarioOIDVOBuilder.esJerarquico != null ? usuarioOIDVOBuilder.esJerarquico : "NO";
		this.clave = usuarioOIDVOBuilder.clave;
		this.rol = usuarioOIDVOBuilder.rol;
		this.codigo = usuarioOIDVOBuilder.codigo;
		this.email = usuarioOIDVOBuilder.email;
		this.nombre = usuarioOIDVOBuilder.nombre;
		this.codigoCliente = usuarioOIDVOBuilder.codigoCliente;
	}

	public UsuarioOIDVO() {
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigoOrigen() {
		return codigoOrigen;
	}

	public void setCodigoOrigen(String codigoOrigen) {
		this.codigoOrigen = codigoOrigen;
	}

	public String getCodigoProductor() {
		return codigoProductor;
	}

	public void setCodigoProductor(String codigoProductor) {
		this.codigoProductor = codigoProductor;
	}

	public String getCodigoEntidad() {
		return codigoEntidad;
	}

	public void setCodigoEntidad(String codigoEntidad) {
		this.codigoEntidad = codigoEntidad;
	}

	public String getCodigoPtoVenta() {
		return codigoPtoVenta;
	}

	public void setCodigoPtoVenta(String codigoPtoVenta) {
		this.codigoPtoVenta = codigoPtoVenta;
	}

	public String getCodigoProveedor() {
		return codigoProveedor;
	}

	public void setCodigoProveedor(String codigoProveedor) {
		this.codigoProveedor = codigoProveedor;
	}

	public String getCodigoUsuarioEntidad() {
		return codigoUsuarioEntidad;
	}

	public void setCodigoUsuarioEntidad(String codigoUsuarioEntidad) {
		this.codigoUsuarioEntidad = codigoUsuarioEntidad;
	}

	public String getEsJerarquico() {
		return esJerarquico;
	}

	public void setEsJerarquico(String esJerarquico) {
		this.esJerarquico = esJerarquico;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getSucursalId() {
		return sucursalId;
	}

	public void setSucursalId(String sucursalId) {
		this.sucursalId = sucursalId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getErrorCodigo() {
		return errorCodigo;
	}

	public void setErrorCodigo(String errorCodigo) {
		this.errorCodigo = errorCodigo;
	}

	public String getErrorMensaje() {
		return errorMensaje;
	}

	public void setErrorMensaje(String errorMensaje) {
		this.errorMensaje = errorMensaje;
	}

	public HashMap<String, String> getPerfiles() {
		return perfiles;
	}

	public void setPerfiles(HashMap<String, String> perfiles) {
		this.perfiles = perfiles;
	}

	@Override
	public String toString() {
		return "UsuarioOIDVO [codigo=" + codigo + ", codigoOrigen=" + codigoOrigen + ", codigoProductor="
				+ codigoProductor + ", codigoEntidad=" + codigoEntidad + ", codigoPtoVenta=" + codigoPtoVenta
				+ ", codigoProveedor=" + codigoProveedor + ", codigoUsuarioEntidad=" + codigoUsuarioEntidad
				+ ", esJerarquico=" + esJerarquico + ", clave=" + clave + ", nombre=" + nombre + ", sucursal="
				+ sucursal + ", sucursalId=" + sucursalId + ", email=" + email + ", rol=" + rol + ", operacion="
				+ operacion + ", perfiles=" + perfiles + ", errorCodigo=" + errorCodigo + ", errorMensaje="
				+ errorMensaje + "]";
	}

	public static UsuarioOIDVOBuilder builder() {
		return new UsuarioOIDVOBuilder();
	}

	public static class UsuarioOIDVOBuilder {

		private String rol;
		private String codigo;
		private String email;
		private String nombre;
		private String clave;
		private String esJerarquico;
		private String codigoCliente;

		public UsuarioOIDVOBuilder setCodigoCliente(String codigoCliente) {
			this.codigoCliente = codigoCliente;
			return this;
		}

		public UsuarioOIDVOBuilder setRol(String rolIdCliente) {
			this.rol = rolIdCliente;
			return this;
		}

		public UsuarioOIDVOBuilder setCodigo(String codigo) {
			this.codigo = codigo;
			return this;
		}

		public UsuarioOIDVOBuilder setEmail(String email) {
			this.email = email;
			return this;
		}

		public UsuarioOIDVOBuilder setNombre(String nombre) {
			this.nombre = nombre;
			return this;
		}

		public UsuarioOIDVOBuilder setClave(String clave) {
			this.clave = clave;
			return this;
		}

		public UsuarioOIDVOBuilder setEsJerarquico(String esJerarquico) {
			this.esJerarquico = esJerarquico;
			return this;
		}

		public UsuarioOIDVO build() {
			return new UsuarioOIDVO(this);
		}
	}

	public String getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
}
