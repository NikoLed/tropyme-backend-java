package com.ps.tropyme.payload;

public class Totales {

    private String sumaAseguradaTotal;
    private String primaTotal;
    private String premioTotal;

    public String getSumaAseguradaTotal() {
        return sumaAseguradaTotal;
    }

    public void setSumaAseguradaTotal(String sumaAseguradaTotal) {
        this.sumaAseguradaTotal = sumaAseguradaTotal;
    }

    public String getPrimaTotal() {
        return primaTotal;
    }

    public void setPrimaTotal(String primaTotal) {
        this.primaTotal = primaTotal;
    }

    public String getPremioTotal() {
        return premioTotal;
    }

    public void setPremioTotal(String premioTotal) {
        this.premioTotal = premioTotal;
    }

}
