package com.ps.tropyme.payload;

import java.io.Serializable;
import java.util.List;

public class PropuestaCotizacionTropyme implements Serializable {

	private static final long serialVersionUID = 2411420059719160486L;

	private CabeceraTropyme cabecera;
	private List<DatosDelRiesgoTropyme> ubicaciones;

	public PropuestaCotizacionTropyme() {
		super();
	}

	public CabeceraTropyme getCabecera() {
		return cabecera;
	}

	public void setCabecera(CabeceraTropyme cabecera) {
		this.cabecera = cabecera;
	}

	public List<DatosDelRiesgoTropyme> getUbicaciones() {
		return ubicaciones;
	}

	public void setUbicaciones(List<DatosDelRiesgoTropyme> ubicaciones) {
		this.ubicaciones = ubicaciones;
	}
}
