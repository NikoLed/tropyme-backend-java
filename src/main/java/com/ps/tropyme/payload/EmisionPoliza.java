package com.ps.tropyme.payload;

public class EmisionPoliza {

    private Boolean yaEmitido;
    private Emision emision;
    private Totales totales;

    public Boolean getYaEmitido() {
        return yaEmitido;
    }

    public void setYaEmitido(Boolean yaEmitido) {
        this.yaEmitido = yaEmitido;
    }

    public Emision getEmision() {
        return emision;
    }

    public void setEmision(Emision emision) {
        this.emision = emision;
    }

    public Totales getTotales() {
        return totales;
    }

    public void setTotales(Totales totales) {
        this.totales = totales;
    }

}
