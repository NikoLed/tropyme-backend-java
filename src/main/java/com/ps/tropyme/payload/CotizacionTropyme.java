package com.ps.tropyme.payload;

import java.io.Serializable;

public class CotizacionTropyme implements Serializable {

	private static final long serialVersionUID = -826212369088139990L;

	private String nroCotizacion;
	private String prima;
	private String premio;

	public CotizacionTropyme() {
		super();
	}

	public String getNroCotizacion() {
		return nroCotizacion;
	}

	public void setNroCotizacion(String nroCotizacion) {
		this.nroCotizacion = nroCotizacion;
	}

	public String getPrima() {
		return prima;
	}

	public void setPrima(String prima) {
		this.prima = prima;
	}

	public String getPremio() {
		return premio;
	}

	public void setPremio(String premio) {
		this.premio = premio;
	}
}
