package com.ps.tropyme.payload;

import java.io.Serializable;

public class ResponseTropyme implements Serializable {

	private static final long serialVersionUID = 3818874450612497836L;

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
