package com.ps.tropyme.payload;

import java.io.Serializable;

public class DatosDelRiesgoTropyme implements Serializable {

	private static final long serialVersionUID = -8964039740940112301L;

	private String id;
	private String ubicacion;
	private String riesgo;
	private String actividad;
	private String descripcionActividad;
	private String cantidadEmpleados;
	private String tipoConstruccion;
	private String superficie;
	private String disyuntor;
	private String instalacionElectricaApropiada;
	private String prohibidoFumar;
	private String matafuegosReglamentarios;
	private String alarma;
	private String hidrantes;
	private String detectoresHidrantes;
	private String paredesCortafuego;	
	private String tanqueAgua;
	private String incendioEdificio;
	private String incendioContenidoGeneral;
	
	public DatosDelRiesgoTropyme() {
		super();
	}
	
	public String getUbicacion() {
		return ubicacion;
	}
	
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	public String getRiesgo() {
		return riesgo;
	}
	public void setRiesgo(String riesgo) {
		this.riesgo = riesgo;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public String getDescripcionActividad() {
		return descripcionActividad;
	}
	public void setDescripcionActividad(String descripcionActividad) {
		this.descripcionActividad = descripcionActividad;
	}
	public String getCantidadEmpleados() {
		return cantidadEmpleados;
	}
	public void setCantidadEmpleados(String cantidadEmpleados) {
		this.cantidadEmpleados = cantidadEmpleados;
	}
	public String getTipoConstruccion() {
		return tipoConstruccion;
	}
	public void setTipoConstruccion(String tipoConstruccion) {
		this.tipoConstruccion = tipoConstruccion;
	}
	public String getDisyuntor() {
		return disyuntor;
	}
	public void setDisyuntor(String disyuntor) {
		this.disyuntor = disyuntor;
	}
	public String getAlarma() {
		return alarma;
	}
	public void setAlarma(String alarma) {
		this.alarma = alarma;
	}
	public String getHidrantes() {
		return hidrantes;
	}
	public void setHidrantes(String hidrantes) {
		this.hidrantes = hidrantes;
	}
	public String getDetectoresHidrantes() {
		return detectoresHidrantes;
	}
	public void setDetectoresHidrantes(String detectoresHidrantes) {
		this.detectoresHidrantes = detectoresHidrantes;
	}
	public String getIncendioEdificio() {
		return incendioEdificio;
	}
	public void setIncendioEdificio(String incendioEdificio) {
		this.incendioEdificio = incendioEdificio;
	}
	public String getIncendioContenidoGeneral() {
		return incendioContenidoGeneral;
	}
	public void setIncendioContenidoGeneral(String incendioContenidoGeneral) {
		this.incendioContenidoGeneral = incendioContenidoGeneral;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSuperficie() {
		return superficie;
	}

	public void setSuperficie(String superficie) {
		this.superficie = superficie;
	}

	public String getInstalacionElectricaApropiada() {
		return instalacionElectricaApropiada;
	}

	public void setInstalacionElectricaApropiada(String instalacionElectricaApropiada) {
		this.instalacionElectricaApropiada = instalacionElectricaApropiada;
	}

	public String getProhibidoFumar() {
		return prohibidoFumar;
	}

	public void setProhibidoFumar(String prohibidoFumar) {
		this.prohibidoFumar = prohibidoFumar;
	}

	public String getMatafuegosReglamentarios() {
		return matafuegosReglamentarios;
	}

	public void setMatafuegosReglamentarios(String matafuegosReglamentarios) {
		this.matafuegosReglamentarios = matafuegosReglamentarios;
	}

	public String getParedesCortafuego() {
		return paredesCortafuego;
	}

	public void setParedesCortafuego(String paredesCortafuego) {
		this.paredesCortafuego = paredesCortafuego;
	}

	public String getTanqueAgua() {
		return tanqueAgua;
	}

	public void setTanqueAgua(String tanqueAgua) {
		this.tanqueAgua = tanqueAgua;
	}

	
}
