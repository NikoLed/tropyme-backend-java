package com.ps.tropyme.payload;

import java.io.Serializable;

public class PropuestaCotizacionResponse implements Serializable {

	private static final long serialVersionUID = -5799051432835664771L;

	private PropuestaCotizacionTropyme propuesta;
	private String token;
	
	public PropuestaCotizacionResponse(PropuestaCotizacionTropyme propuesta) {
		super();
		this.propuesta = propuesta;
	}

	public PropuestaCotizacionTropyme getPropuesta() {
		return propuesta;
	}

	public void setPropuesta(PropuestaCotizacionTropyme propuesta) {
		this.propuesta = propuesta;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
