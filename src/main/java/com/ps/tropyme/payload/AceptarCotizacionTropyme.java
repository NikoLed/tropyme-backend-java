package com.ps.tropyme.payload;

import java.io.Serializable;

public class AceptarCotizacionTropyme implements Serializable {

	private static final long serialVersionUID = -5623926638175451310L;

	private String token;
	private Integer cotizacion;
	private String prima;
	private String premio;

	public AceptarCotizacionTropyme() {
		super();
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Integer getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(Integer cotizacion) {
		this.cotizacion = cotizacion;
	}

	public String getPrima() {
		return prima;
	}

	public void setPrima(String prima) {
		this.prima = prima;
	}

	public String getPremio() {
		return premio;
	}

	public void setPremio(String premio) {
		this.premio = premio;
	}
}
