package com.ps.tropyme.payload;

import java.io.Serializable;

public class SolicitudEmisionTropyme implements Serializable {

	private static final long serialVersionUID = 5981775822077674069L;

	private Integer id;
	private String email;
	private String token;
	private String medioDePago;
	private String nroCbuTarjeta;
	private String fechaVencimiento;
	private String origenDePago;
	private String cantidadDeCuotas;

	public SolicitudEmisionTropyme() {
		super();
	}

	public String getMedioDePago() {
		return medioDePago;
	}

	public void setMedioDePago(String medioDePago) {
		this.medioDePago = medioDePago;
	}

	public String getNroCbuTarjeta() {
		return nroCbuTarjeta;
	}

	public void setNroCbuTarjeta(String nroCbuTarjeta) {
		this.nroCbuTarjeta = nroCbuTarjeta;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getOrigenDePago() {
		return origenDePago;
	}

	public void setOrigenDePago(String origenDePago) {
		this.origenDePago = origenDePago;
	}

	public String getCantidadDeCuotas() {
		return cantidadDeCuotas;
	}

	public void setCantidadDeCuotas(String cantidadDeCuotas) {
		this.cantidadDeCuotas = cantidadDeCuotas;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
