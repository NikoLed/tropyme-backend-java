package com.ps.tropyme.payload;

import java.io.Serializable;

public class RechazarCotizacionTropyme implements Serializable {

	private static final long serialVersionUID = -8000315524593455973L;

	private String token;
	
	public RechazarCotizacionTropyme() {
		super();
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
