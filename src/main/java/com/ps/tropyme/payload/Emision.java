package com.ps.tropyme.payload;

public class Emision {

    private String nroCbuTarjeta;
    private String fechaVencimiento;

    public String getNroCbuTarjeta() {
        return nroCbuTarjeta;
    }

    public void setNroCbuTarjeta(String nroCbuTarjeta) {
        this.nroCbuTarjeta = nroCbuTarjeta;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

}
