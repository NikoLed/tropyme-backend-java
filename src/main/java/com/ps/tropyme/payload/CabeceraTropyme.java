package com.ps.tropyme.payload;

import java.io.Serializable;

public class CabeceraTropyme implements Serializable {

	private static final long serialVersionUID = 2154463840805531124L;

	private Integer id;
	private String token;
	private String productor;
	private String promocion;
	private String periodo;
	private String cuit;
	private String moneda;
	private String razonSocial;
	private String tipoPersona;
	private String ingresosBruto;
	private String iva;
	private String mail;
	private String mailProductor;
	private String telefono;
	private String medioDePago;
	private String origenDePago;
	private String cantidadDeCuotas;
	private String domicilioLegal;
	private EmisionPoliza emisionPoliza;
	
	public CabeceraTropyme() {
		super();
	}

	public String getProductor() {
		return productor;
	}

	public void setProductor(String productor) {
		this.productor = productor;
	}

	public String getPromocion() {
		return promocion;
	}

	public void setPromocion(String promocion) {
		this.promocion = promocion;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getDomicilioLegal() {
		return domicilioLegal;
	}

	public void setDomicilioLegal(String domicilioLegal) {
		this.domicilioLegal = domicilioLegal;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getIngresosBruto() {
		return ingresosBruto;
	}

	public void setIngresosBruto(String ingresosBruto) {
		this.ingresosBruto = ingresosBruto;
	}

	public String getIva() {
		return iva;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getMedioDePago() {
		return medioDePago;
	}

	public void setMedioDePago(String medioDePago) {
		this.medioDePago = medioDePago;
	}

	public String getOrigenDePago() {
		return origenDePago;
	}

	public void setOrigenDePago(String origenDePago) {
		this.origenDePago = origenDePago;
	}

	public String getCantidadDeCuotas() {
		return cantidadDeCuotas;
	}

	public void setCantidadDeCuotas(String cantidadDeCuotas) {
		this.cantidadDeCuotas = cantidadDeCuotas;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMailProductor() {
		return mailProductor;
	}

	public void setMailProductor(String mailProductor) {
		this.mailProductor = mailProductor;
	}

    public EmisionPoliza getEmisionPoliza() {
        return emisionPoliza;
    }

    public void setEmisionPoliza(EmisionPoliza emisionPoliza) {
        this.emisionPoliza = emisionPoliza;
    }
}
