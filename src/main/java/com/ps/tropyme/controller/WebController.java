package com.ps.tropyme.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ps.tropyme.exception.CotizacionNotFoundException;
import com.ps.tropyme.payload.AceptarCotizacionTropyme;
import com.ps.tropyme.payload.CotizacionTropyme;
import com.ps.tropyme.payload.PropuestaCotizacionResponse;
import com.ps.tropyme.payload.PropuestaCotizacionTropyme;
import com.ps.tropyme.payload.RechazarCotizacionTropyme;
import com.ps.tropyme.payload.ResponseTropyme;
import com.ps.tropyme.payload.SolicitudEmisionTropyme;
import com.ps.tropyme.service.TropymeService;

@RestController
public class WebController {

	@Autowired
	private TropymeService service;
	
	@CrossOrigin(origins= {"*"})
	@RequestMapping(value = "/solicitudTropyme", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody PropuestaCotizacionResponse cargarSolicitudTropyme(@RequestBody PropuestaCotizacionTropyme cotizacion) throws Exception {
		return service.solicitudTropyme(cotizacion);
	}
	
	@CrossOrigin(origins= {"*"})
	@RequestMapping(value = "/datosSolicitudTropyme", method = RequestMethod.GET, produces = "application/json")
	public PropuestaCotizacionTropyme getSolicitudTropyme(@RequestParam String param) {
		PropuestaCotizacionTropyme propuestaCotizacionTropymeTropyme = service.getSolicitudTropyme(param);
		if (propuestaCotizacionTropymeTropyme == null) {
			throw new CotizacionNotFoundException();
		}
		return propuestaCotizacionTropymeTropyme;
	}
	
	@CrossOrigin(origins= {"*"})
	@RequestMapping(value = "/aceptarSolicitudTropyme", method = RequestMethod.POST, produces = "application/json")
	public ResponseTropyme aceptarSolicitudTropyme(@RequestBody AceptarCotizacionTropyme aceptacion) {
		return this.service.aceptarSolicitudTropyme(aceptacion);
	}
	
	@CrossOrigin(origins= {"*"})
	@RequestMapping(value = "/rechazarSolicitudTropyme", method = RequestMethod.POST, produces = "application/json")
	public ResponseTropyme rechazarSolicitudTropyme(@RequestBody RechazarCotizacionTropyme rechazo) {
		return this.service.rechazarSolicitudTropyme(rechazo);
	}
	
	@CrossOrigin(origins= {"*"})
	@RequestMapping(value = "/resultadoCotizacionTropyme", method = RequestMethod.GET, produces = "application/json")
	public CotizacionTropyme getDatosCotizacion(@RequestParam String nuCotizacion) {
		CotizacionTropyme cotizacionWebTropyme = service.getCotizacionWebTropyme(nuCotizacion);
		if (cotizacionWebTropyme == null) {
			throw new CotizacionNotFoundException();
		}
		cotizacionWebTropyme.setNroCotizacion(nuCotizacion);
		return cotizacionWebTropyme;
	}

	@CrossOrigin(origins= {"*"})
	@RequestMapping(value = "/solicitudEmisionTropyme", method = RequestMethod.POST, produces = "application/json")
	public ResponseTropyme cargarSolicitudEmisionTropyme(@RequestBody SolicitudEmisionTropyme  solicitudEmisionTropyme) throws Exception {
		return service.solicitudEmisionTropyme(solicitudEmisionTropyme);
	}
	
}
