package com.ps.tropyme.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ps.tropyme.payload.SolicitudEmisionTropyme;
import com.ps.tropyme.util.TemplateUtils;

@Service
public class MailHelper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MailHelper.class);

	@Value("${mail.host}")
	private String host;

	@Value("${mail.port}")
	private String port;

	@Value("${mail.username}")
	private String username;

	@Value("${mail.password}")
	private String password;

	@Value("${mail.to}")
	private String to;

	@Value("${mail.reply}")
	private String reply;

	@Value("${mail.urlCargaCotizacion}")
	private String urlCargaCotizacion;
	
	@Value("${mail.urlCargaAprobacion}")
    private String urlCargaAprobacion;
    
	public void sendSolicitudOperador(String token) {
		LOGGER.debug("sendSolicitudOperador-token: " + token);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("urlSolicitudTropyme", urlCargaCotizacion + token);
		sendEmail(to, "Seguros PYME - Solitud de Cotización", TemplateUtils.getBodySolicitudTropyme(map));
	}

	public void sendEmailAprobacion(String token) {
		String body = urlCargaAprobacion + token;
		sendEmail(to, "Seguros PYME Cotización de Solicitud", body);
	}

	public void sendEmailRechazo(String token) {
		String body = urlCargaCotizacion + token;
		sendEmail(to, "Seguros PYME Rechazo de Solicitud", body);
	}

	public void sendEmail(String toEmail, String subject, String body) {
		try {
			
			LOGGER.debug("sendEmail.to: " + toEmail);
			LOGGER.debug("sendEmail.subject: " + subject);
			
			Properties props = System.getProperties();
			props.put("mail.smtp.host", host);
			Session session = Session.getInstance(props, null);

			MimeMessage msg = new MimeMessage(session);
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");

			msg.setFrom(new InternetAddress(username));

			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
			if (reply != null && reply.length() > 1) {
				msg.setReplyTo(InternetAddress.parse(reply, false));

			}
			msg.setSubject(subject, "UTF-8");
			msg.setContent(body, "text/HTML; charset=UTF-8");

			msg.setSentDate(new Date());
			Transport.send(msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendEmailSolicitudEmision(SolicitudEmisionTropyme solicitudEmisionTropyme) {
		String url = urlCargaCotizacion + solicitudEmisionTropyme.getToken();
		String body = url;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("urlSolicitudTropyme", url);
		sendEmail(to, "Seguros PYME - Solitud de Emision", TemplateUtils.getBodySolicitudTropyme(map));
		//sendEmail(to, "Seguros PYME - Solitud de Emisión", body);
	}

}
