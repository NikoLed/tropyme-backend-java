package com.ps.tropyme.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ps.tropyme.exception.ProductorNotFoundException;
import com.ps.tropyme.oid.OIDConnector;
import com.ps.tropyme.oid.vo.UsuarioOIDVO;
import com.ps.tropyme.payload.AceptarCotizacionTropyme;
import com.ps.tropyme.payload.CotizacionTropyme;
import com.ps.tropyme.payload.EmisionPoliza;
import com.ps.tropyme.payload.PropuestaCotizacionResponse;
import com.ps.tropyme.payload.PropuestaCotizacionTropyme;
import com.ps.tropyme.payload.RechazarCotizacionTropyme;
import com.ps.tropyme.payload.ResponseTropyme;
import com.ps.tropyme.payload.SolicitudEmisionTropyme;
import com.ps.tropyme.repository.TropymeRepository;
import com.ps.tropyme.util.JwtUtils;

@Service
@Transactional
public class TropymeService {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(TropymeService.class);
	
	@Autowired
	private TropymeRepository repository;

	@Autowired
	private MailHelper mailHelper;
	
    @Autowired
    private OIDConnector oidConnector;

	/**
	 * persiste la solicitud de Seguros Tropyme realizada por la web
	 * si el nro de productor ingresado es incorrecto se lanza una exception
	 * @param cotizacion
	 * @return
	 * @throws Exception
	 */
	public PropuestaCotizacionResponse solicitudTropyme(PropuestaCotizacionTropyme cotizacion) throws Exception {
		
		if (!oidConnector.existeUsuario(cotizacion.getCabecera().getProductor())) {
			throw new ProductorNotFoundException("El productor informado no está registrado");
		}
		
		UsuarioOIDVO usuarioOIDVOByExample = UsuarioOIDVO.builder().setCodigo(cotizacion.getCabecera().getProductor()).build();
		UsuarioOIDVO usuarioFromOID = oidConnector.getUsuarioFromOID(usuarioOIDVOByExample);
		
		String token = JwtUtils.getToken(cotizacion.getCabecera().getProductor());
		
		LOGGER.debug("solicitudTropyme-token: "  + token);
		
		cotizacion.getCabecera().setMailProductor(usuarioFromOID.getEmail());
		repository.saveCotizacionTropyme(cotizacion, token);
		mailHelper.sendSolicitudOperador(token);
		
		LOGGER.debug("solicitudTropyme-fin: ");
		
		
		PropuestaCotizacionResponse propuestaCotizacionResponse = new PropuestaCotizacionResponse(cotizacion);
		propuestaCotizacionResponse.setToken(token);
		return propuestaCotizacionResponse;
	}

	/**
	 * retorna los datos de solicitud de seguros Tropyme
	 * @param id
	 * @return
	 */
	public PropuestaCotizacionTropyme getSolicitudTropyme(String id) {
	    PropuestaCotizacionTropyme  propuestaCotizacionTropyme= repository.getCotizacionTropyme(id);
		return propuestaCotizacionTropyme;
	}

	/**
	 * Envia la respuesta al productor indicando que la solicutud fue rechazada
	 * marca la solicitud con dicho estado (RECHAZADA)
	 * @param rechazo
	 * @return
	 */
	public ResponseTropyme rechazarSolicitudTropyme(RechazarCotizacionTropyme rechazo) {
		repository.rechazarSolicitudTropyme(rechazo.getToken());
		mailHelper.sendEmailRechazo(rechazo.getToken());
		return new ResponseTropyme();
	}

	public ResponseTropyme aceptarSolicitudTropyme(AceptarCotizacionTropyme aceptacion) {
		repository.saveCoticacionTropyme(aceptacion);
	    mailHelper.sendEmailAprobacion(aceptacion.getToken());
		return new ResponseTropyme();
	}

	public CotizacionTropyme getCotizacionWebTropyme(String nuCotizacion) {
		return repository.getCotizacionWebTropyme(nuCotizacion);
	}

	public OIDConnector getOidConnector() {
		return oidConnector;
	}

	public void setOidConnector(OIDConnector oidConnector) {
		this.oidConnector = oidConnector;
	}

	public ResponseTropyme solicitudEmisionTropyme(SolicitudEmisionTropyme solicitudEmisionTropyme) {
		repository.solicitudEmisionTropyme(solicitudEmisionTropyme);
		mailHelper.sendEmailSolicitudEmision(solicitudEmisionTropyme);
		return new ResponseTropyme();
	}
}
