package com.ps.tropyme.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ProductorNotFoundException extends RuntimeException {

	public ProductorNotFoundException(String message) {
		super(message);
	}

	private static final long serialVersionUID = -7462851928732410580L;

}
