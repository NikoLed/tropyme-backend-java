package com.ps.tropyme.repository.jdbc.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ps.tropyme.payload.CotizacionTropyme;

public class CotizacionWebTropymeMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		CotizacionTropyme cotizacion = new CotizacionTropyme();
		cotizacion.setPremio(rs.getString("CAZB_MT_PREMIO"));
		cotizacion.setPrima(rs.getString("CAZB_MT_PREMIO"));
		return cotizacion;
	}

}
