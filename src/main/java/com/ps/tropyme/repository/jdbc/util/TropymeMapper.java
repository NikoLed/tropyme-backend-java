package com.ps.tropyme.repository.jdbc.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ps.tropyme.payload.CabeceraTropyme;
import com.ps.tropyme.payload.Emision;
import com.ps.tropyme.payload.EmisionPoliza;
import com.ps.tropyme.payload.Totales;

public class TropymeMapper implements RowMapper {

    // TODO:WETR_NU_COTIZACION
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        
        Totales totales = new Totales();
        totales.setPremioTotal(rs.getString("WETR_PREMIO"));
        totales.setPrimaTotal(rs.getString("WETR_PRIMA"));
        
        EmisionPoliza emisionPoliza = new EmisionPoliza();
        emisionPoliza.setYaEmitido(rs.getInt("WTTRE_EMITIDO") == 1);
        emisionPoliza.setTotales(totales);
        
        Emision emision = new Emision();
        emision.setFechaVencimiento(rs.getString("WTTRE_FECHA_VENCIMIENTO"));
        emision.setNroCbuTarjeta(rs.getString("WTTRE_NRO_MDPAGO"));
        emisionPoliza.setEmision(emision);
        
        CabeceraTropyme cabeceraTropyme = new CabeceraTropyme();
        cabeceraTropyme.setId(rs.getInt("WETR_ID"));
        cabeceraTropyme.setToken(rs.getString("WETR_TOKEN"));
        cabeceraTropyme.setProductor(rs.getString("WETR_PRODUCTOR"));
        cabeceraTropyme.setPromocion(rs.getString("WETR_PROMOCION"));
        cabeceraTropyme.setPeriodo(rs.getString("WETR_PERIODO"));
        cabeceraTropyme.setCuit(rs.getString("WETR_CUIT"));
        cabeceraTropyme.setMoneda(rs.getString("WETR_MONEDA"));
        cabeceraTropyme.setRazonSocial(rs.getString("WETR_RAZONSOCIAL"));
        cabeceraTropyme.setTipoPersona(rs.getString("WETR_TIPOPERSONA"));
        cabeceraTropyme.setIngresosBruto(rs.getString("WETR_INGRESOSBRUTO"));
        cabeceraTropyme.setIva(rs.getString("WETR_IVA"));
        cabeceraTropyme.setMail(rs.getString("WETR_MAIL"));
        cabeceraTropyme.setTelefono(rs.getString("WETR_TELEFONO"));
        cabeceraTropyme.setMedioDePago(rs.getString("WETR_MEDIODEPAGO"));
        cabeceraTropyme.setOrigenDePago(rs.getString("WETR_ORIGENDEPAGO"));
        cabeceraTropyme.setCantidadDeCuotas(rs.getString("WETR_CANTIDADDECUOTAS"));
        cabeceraTropyme.setDomicilioLegal(rs.getString("WETR_DOMICILIOLEGAL"));
        cabeceraTropyme.setEmisionPoliza(emisionPoliza);
        
        return cabeceraTropyme;
    }

}
