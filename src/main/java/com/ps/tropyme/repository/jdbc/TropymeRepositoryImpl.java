package com.ps.tropyme.repository.jdbc;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ps.tropyme.payload.AceptarCotizacionTropyme;
import com.ps.tropyme.payload.CabeceraTropyme;
import com.ps.tropyme.payload.CotizacionTropyme;
import com.ps.tropyme.payload.DatosDelRiesgoTropyme;
import com.ps.tropyme.payload.PropuestaCotizacionTropyme;
import com.ps.tropyme.payload.SolicitudEmisionTropyme;
import com.ps.tropyme.repository.TropymeRepository;
import com.ps.tropyme.repository.jdbc.util.CotizacionWebTropymeMapper;
import com.ps.tropyme.repository.jdbc.util.TropymeMapper;
import com.ps.tropyme.repository.jdbc.util.TropymeUbicacionMapper;

@Repository
public class TropymeRepositoryImpl implements TropymeRepository {

    protected JdbcTemplate jdbcTemplate;

    private static final Logger LOGGER = LoggerFactory.getLogger(TropymeRepositoryImpl.class);

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static HashMap<String, PropuestaCotizacionTropyme> dummy = new HashMap<String, PropuestaCotizacionTropyme>();

    public void saveCotizacionTropyme(PropuestaCotizacionTropyme cotizacion, String token) {
        try {
            Integer id = (Integer) jdbcTemplate.queryForObject("select WEBT_TROPYME_SEQ.NEXTVAL from dual",
                    new Object[] {}, Integer.class);
            saveCabecera(cotizacion, token, id);
            saveUbicaciones(cotizacion, id);
        } catch (Exception e) {
            LOGGER.error("saveCotizacionTropyme", e);
            e.printStackTrace();
        }
    }

    private void saveUbicaciones(PropuestaCotizacionTropyme cotizacion, Integer id) {
        for (DatosDelRiesgoTropyme ubicacion : cotizacion.getUbicaciones()) {
            Object[] params = { id, ubicacion.getId(), ubicacion.getUbicacion(), ubicacion.getRiesgo(),
                    ubicacion.getActividad(), ubicacion.getDescripcionActividad(), ubicacion.getCantidadEmpleados(),
                    ubicacion.getTipoConstruccion(), ubicacion.getSuperficie(), ubicacion.getDisyuntor(),
                    ubicacion.getInstalacionElectricaApropiada(), ubicacion.getProhibidoFumar(),
                    ubicacion.getMatafuegosReglamentarios(), ubicacion.getAlarma(), ubicacion.getHidrantes(),
                    ubicacion.getDetectoresHidrantes(), ubicacion.getParedesCortafuego(), ubicacion.getTanqueAgua(),
                    ubicacion.getIncendioEdificio(), ubicacion.getIncendioContenidoGeneral() };
            int[] types = { Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
                    Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
                    Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
                    Types.VARCHAR, Types.VARCHAR };
            StringBuilder insert = new StringBuilder().append("insert into WEBT_TROPYME_UBICACION ")
                    .append("(WETU_ID, WETU_CONSECUTIVO, WETU_UBICACION, ")
                    .append(" WETU_RIESGO, WETU_ACTIVIDAD, WETU_DESCRIPCIONACTIVIDAD,	")
                    .append(" WETU_CANTIDADEMPLEADOS, WETU_TIPOCONSTRUCCION, WETU_SUPERFIE, ")
                    .append(" WETU_DISYUNTOR, WETU_INSTALACIONELECTRICA, WETU_PROHIBOFUMAR,	")
                    .append(" WETU_MATAFUEGO, WETU_ALARMA, WETU_HIDRANTES, WETU_DETECTORESHIDRANTES,	")
                    .append(" WETU_PAREDES, WETU_TANQUE, WETU_INCENDIOEDIFICIO, WETU_INCENDIOCONTENIDOGENERAL) ")
                    .append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

            int update = jdbcTemplate.update(insert.toString(), params, types);
            LOGGER.debug("saveUbicaciones - update: " + update);
        }
    }

    private void saveCabecera(PropuestaCotizacionTropyme cotizacion, String token, Integer id) {
        Object[] params = { id, token, cotizacion.getCabecera().getProductor(), cotizacion.getCabecera().getPromocion(),
                cotizacion.getCabecera().getPeriodo(), cotizacion.getCabecera().getCuit(),
                cotizacion.getCabecera().getMoneda(), cotizacion.getCabecera().getRazonSocial(),
                cotizacion.getCabecera().getTipoPersona(), cotizacion.getCabecera().getIngresosBruto(),
                cotizacion.getCabecera().getIva(), cotizacion.getCabecera().getMail(),
                cotizacion.getCabecera().getTelefono(), cotizacion.getCabecera().getMedioDePago(),
                cotizacion.getCabecera().getOrigenDePago(), cotizacion.getCabecera().getCantidadDeCuotas(),
                cotizacion.getCabecera().getDomicilioLegal() };
        int[] types = { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
                Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
                Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };
        StringBuilder insert = new StringBuilder().append("insert into WEBT_TROPYME(WETR_ID,")
                .append("WETR_TOKEN, WETR_PRODUCTOR, WETR_PROMOCION,")
                .append("WETR_PERIODO, WETR_CUIT, WETR_MONEDA, WETR_RAZONSOCIAL,")
                .append("WETR_TIPOPERSONA, WETR_INGRESOSBRUTO, WETR_IVA,")
                .append("WETR_MAIL, WETR_TELEFONO, WETR_MEDIODEPAGO, WETR_ORIGENDEPAGO,")
                .append("WETR_CANTIDADDECUOTAS, WETR_DOMICILIOLEGAL, WETR_FECHA_INGRESO) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, sysdate) ");

        jdbcTemplate.update(insert.toString(), params, types);
    }

    public PropuestaCotizacionTropyme getCotizacionTropyme(String token) {
        try {
            CabeceraTropyme cabecera = getCabecera(token);
            List<DatosDelRiesgoTropyme> ubicaciones = getUbicaciones(cabecera);
            PropuestaCotizacionTropyme propuestaCotizacionTropyme = new PropuestaCotizacionTropyme();
            propuestaCotizacionTropyme.setCabecera(cabecera);
            propuestaCotizacionTropyme.setUbicaciones(ubicaciones);
            return propuestaCotizacionTropyme;
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(" getCotizacionTropyme ", e);
        }
        return null;
    }

    private List<DatosDelRiesgoTropyme> getUbicaciones(CabeceraTropyme cabecera) {
        StringBuilder select = new StringBuilder().append("select    ")
                .append("WETU_ID, WETU_CONSECUTIVO, WETU_UBICACION, ")
                .append("WETU_RIESGO, WETU_ACTIVIDAD	, WETU_DESCRIPCIONACTIVIDAD, ")
                .append("WETU_CANTIDADEMPLEADOS, WETU_TIPOCONSTRUCCION, WETU_SUPERFIE, ")
                .append("WETU_DISYUNTOR, WETU_INSTALACIONELECTRICA, WETU_PROHIBOFUMAR, WETU_MATAFUEGO, ")
                .append("WETU_ALARMA, WETU_HIDRANTES, WETU_DETECTORESHIDRANTES, WETU_PAREDES, ")
                .append("WETU_TANQUE, WETU_INCENDIOEDIFICIO, WETU_INCENDIOCONTENIDOGENERAL 	")
                .append("from WEBT_TROPYME_UBICACION where WETU_ID = ?");
        Object[] params = { cabecera.getId() };
        int[] argTypes = { Types.INTEGER };
        List<DatosDelRiesgoTropyme> ubicaciones = (List<DatosDelRiesgoTropyme>) jdbcTemplate.query(select.toString(),
                params, argTypes, new TropymeUbicacionMapper());
        return ubicaciones;
    }

    private CabeceraTropyme getCabecera(String token) {
        StringBuilder select = new StringBuilder().append("select    ")
                .append("WETR_ID, WETR_TOKEN, WETR_PRODUCTOR, WETR_PROMOCION,")
                .append("WETR_PERIODO, WETR_CUIT, WETR_MONEDA, WETR_RAZONSOCIAL,")
                .append("WETR_TIPOPERSONA, WETR_INGRESOSBRUTO, WETR_IVA,")
                .append("WETR_MAIL, WETR_TELEFONO, WETR_MEDIODEPAGO, WETR_ORIGENDEPAGO,")
                .append("WETR_CANTIDADDECUOTAS , WETR_DOMICILIOLEGAL ,WETR_NU_COTIZACION , WETR_PRIMA , WETR_PREMIO,")
                .append("WTTRE_EMITIDO,WTTRE_NRO_MDPAGO,WTTRE_FECHA_VENCIMIENTO ")
                .append("from WEBT_TROPYME where WETR_TOKEN = ?");
        Object[] params = { token };
        int[] argTypes = { Types.VARCHAR };
        CabeceraTropyme cabecera = (CabeceraTropyme) jdbcTemplate.queryForObject(select.toString(), params, argTypes,
                new TropymeMapper());
        return cabecera;
    }

    public CotizacionTropyme getCotizacionWebTropyme(String nuCotizacion) {
        try {
            StringBuilder select = new StringBuilder().append("select    ").append("CAZB_MT_PREMIO ")
                    .append("from cart_cotiza_planes where cazb_nu_cotizacion = ?");
            Object[] params = { Integer.valueOf(nuCotizacion) };
            int[] argTypes = { Types.NUMERIC };
            CotizacionTropyme cotizacion = (CotizacionTropyme) jdbcTemplate.queryForObject(select.toString(), params,
                    argTypes, new CotizacionWebTropymeMapper());
            return cotizacion;
        } catch (Exception e) {
        }
        return null;
    }

    public void rechazarSolicitudTropyme(String token) {
        Object[] params = { token };
        int[] types = { Types.VARCHAR };
        StringBuilder rechazoQuery = new StringBuilder().append("update  WEBT_TROPYME ")
                .append(" set WETU_ESTADO_SOLICITUD = 'RECHAZADA'").append(" where WETR_TOKEN = ?	");

        int update = jdbcTemplate.update(rechazoQuery.toString(), params, types);
        LOGGER.debug("rechazarSolicitudTropyme - update: " + update);
    }

    public void solicitudEmisionTropyme(SolicitudEmisionTropyme solicitudEmisionTropyme) {
        Integer yaEmitido = 1;
        Object[] params = {yaEmitido,solicitudEmisionTropyme.getNroCbuTarjeta(),solicitudEmisionTropyme.getFechaVencimiento(),solicitudEmisionTropyme.getToken()};
        int[] types = { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };
        StringBuilder insert = new StringBuilder().append(
                "UPDATE WEBT_TROPYME SET WTTRE_EMITIDO = ?,WTTRE_NRO_MDPAGO = ? , WTTRE_FECHA_VENCIMIENTO = ? WHERE WETR_TOKEN = ?");
        jdbcTemplate.update(insert.toString(), params, types);
    }

    public void saveCoticacionTropyme(AceptarCotizacionTropyme cotizacinTropyme) {

        Object[] params = { cotizacinTropyme.getCotizacion(), cotizacinTropyme.getPrima(), cotizacinTropyme.getPremio(),
                cotizacinTropyme.getToken() };
        int[] types = { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };
        StringBuilder insert = new StringBuilder().append(
                "UPDATE WEBT_TROPYME SET WETR_NU_COTIZACION = ? , WETR_PRIMA = ?, WETR_PREMIO =? WHERE WETR_TOKEN = ?");
        jdbcTemplate.update(insert.toString(), params, types);
    }

}
