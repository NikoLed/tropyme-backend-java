package com.ps.tropyme.repository.jdbc.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ps.tropyme.payload.DatosDelRiesgoTropyme;

public class TropymeUbicacionMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		DatosDelRiesgoTropyme datosDelRiesgoTropyme = new DatosDelRiesgoTropyme();

		datosDelRiesgoTropyme.setId(rs.getString("WETU_CONSECUTIVO"));
		datosDelRiesgoTropyme.setUbicacion(rs.getString("WETU_UBICACION"));
		datosDelRiesgoTropyme.setRiesgo(rs.getString("WETU_RIESGO"));
		datosDelRiesgoTropyme.setActividad(rs.getString("WETU_ACTIVIDAD"));
		datosDelRiesgoTropyme.setDescripcionActividad(rs.getString("WETU_DESCRIPCIONACTIVIDAD"));
		datosDelRiesgoTropyme.setCantidadEmpleados(rs.getString("WETU_CANTIDADEMPLEADOS"));
		datosDelRiesgoTropyme.setTipoConstruccion(rs.getString("WETU_TIPOCONSTRUCCION"));
		datosDelRiesgoTropyme.setSuperficie(rs.getString("WETU_SUPERFIE"));
		datosDelRiesgoTropyme.setDisyuntor(rs.getString("WETU_DISYUNTOR"));
		datosDelRiesgoTropyme.setInstalacionElectricaApropiada(rs.getString("WETU_INSTALACIONELECTRICA"));
		datosDelRiesgoTropyme.setProhibidoFumar(rs.getString("WETU_PROHIBOFUMAR"));
		datosDelRiesgoTropyme.setMatafuegosReglamentarios(rs.getString("WETU_MATAFUEGO"));
		datosDelRiesgoTropyme.setAlarma(rs.getString("WETU_ALARMA"));
		datosDelRiesgoTropyme.setHidrantes(rs.getString("WETU_HIDRANTES"));
		datosDelRiesgoTropyme.setDetectoresHidrantes(rs.getString("WETU_DETECTORESHIDRANTES"));
		datosDelRiesgoTropyme.setParedesCortafuego(rs.getString("WETU_PAREDES"));
		datosDelRiesgoTropyme.setTanqueAgua(rs.getString("WETU_TANQUE"));
		datosDelRiesgoTropyme.setIncendioEdificio(rs.getString("WETU_INCENDIOEDIFICIO"));
		datosDelRiesgoTropyme.setIncendioContenidoGeneral(rs.getString("WETU_INCENDIOCONTENIDOGENERAL"));

		return datosDelRiesgoTropyme;
	}

}
