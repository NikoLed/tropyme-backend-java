package com.ps.tropyme.repository;

import com.ps.tropyme.payload.AceptarCotizacionTropyme;
import com.ps.tropyme.payload.CotizacionTropyme;
import com.ps.tropyme.payload.PropuestaCotizacionTropyme;
import com.ps.tropyme.payload.SolicitudEmisionTropyme;

public interface TropymeRepository {

	void saveCotizacionTropyme(PropuestaCotizacionTropyme cotizacion, String token);

	PropuestaCotizacionTropyme getCotizacionTropyme(String id);

	CotizacionTropyme getCotizacionWebTropyme(String nuCotizacion);

	void rechazarSolicitudTropyme(String token);

	void solicitudEmisionTropyme(SolicitudEmisionTropyme solicitudEmisionTropyme);
	
	void saveCoticacionTropyme(AceptarCotizacionTropyme cotizacinTropyme);
}
